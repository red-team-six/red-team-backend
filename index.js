const fs = require('fs')
const app = require('express')()
const socketio = require('socket.io')
const bodyParser = require('body-parser')
const cors = require('cors')

const port = process.env.PORT || 3000
const dataFile = './data/outputs.json'

app.use(cors())
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

const server = require('http').createServer(app)
const io = socketio(server)

app.get('/', (req, res) => {
  res.json({ info: 'Node.js, Express, and Postgres API' })
})

app.get('/users', (req, res) => {
  res.status(200).json(require('./data/users.json'))
})

app.get('/services', (req, res) => {
  res.status(200).json(require('./data/services.json'))
})

app.get('/services/:serviceName', (req, res) => {
  const services = require('./data/services.json')
  const service = services.filter(
    service => service.name.toLowerCase() === req.params.serviceName.toLowerCase()
  )

  res.status(200).json(service)
})

app.post('/services/:serviceName/increment', (req, res) => {
  const data = require(dataFile)
  let incData = ''

  console.log('loaded data file', data)

  switch (req.params.serviceName) {
    case 'housing':
      data.requests.housing += 1
      incData = data.requests.housing
      break
    case 'food':
      data.requests.food += 1
      incData = data.requests.food
      break
    case 'jobs':
      data.requests.jobs += 1
      incData = data.requests.job
      break
    default:
      data.engagements += 1
      incData = data.engagements
      break
  }

  fs.writeFile(dataFile, JSON.stringify(data), err => {
    if (err) throw err
    console.log('file saved!')
  })

  io.emit('UPDATE_STATS', data)
  res.status(200).send(`incremented ${req.params.serviceName} to be ${incData}`)
})

app.get('/stats', (req, res) => {
  const data = require(dataFile)

  res.status(200).json(data)
})

server.listen(port, () => {
  console.log(`Server running on port ${port}.`)
})
